const restful = require('node-restful')
const mongoose = restful.mongoose

const deviceSchema = new mongoose.Schema({
    deviceId:{type:String, unique:true},
    key:{type:String}
})

module.exports = restful.model('Device',deviceSchema)