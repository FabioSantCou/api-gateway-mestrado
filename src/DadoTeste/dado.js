const restful = require('node-restful')
const mongoose = restful.mongoose

const dadoSchema = new mongoose.Schema({
    codigo:{type:String, require:true},
    deviceId:{type:String, require:true},
    timestampInicial:{type:String, require:true},
    timestampFinal:{type:String, require:true},
    tempoTotal:{type:Number, require:true},
    mensagem:{type:String, require:true}
})

module.exports = restful.model('Dado',dadoSchema)